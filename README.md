# service-worker-provider

A React library that allows an app to load, update, and communicate with a
service worker. Very useful in a PWA. Handles many of the complex behaviors
needed to keep an application up-to-date with the latest version of a service
worker including:

- the device's online status changes
- the visibility of the application changes
- indicates when a new version of a service worker is available
- allows the app to force the new service worker to be loaded and optionally
  reload the page.

This library does not provide a service worker, it simplifies maintaining a
service worker in a React web app.

## Install

```sh
npm install service-worker-provider
```

-OR-

```sh
yarn add service-worker-provider
```

## Usage

First import the ServiceWorkerProvider component and place it with other React
providers (multiple ServiceWorkerProviders with different service workers are
allowed). Once this is done the following functionality is enabled:

- Check for a new service worker when network status changes from offline to
  online.
- Check for a new service worker when the web page becomes visible (e.g. browser
  is maximized or the page's browser tab gets focus).

Provide the url of the service worker file to fetch and load (relative to the
origin). Setting `reloadOnSkipWaiting` to true means that after the user
manually approves activating a new version of the service worker (see
`skipWaiting`) that the current page will be reloaded. Do this to be certain the
page is loaded using the resources specified in the latest version of the
service worker.

```tsx
import { ServiceWorkerProvider } from "service-worker-provider";

export function App({ children }: Props) {
  return (
    <ServiceWorkerProvider url="service-worker.js" reloadOnSkipWaiting={true}>
      {children}
    </ServiceWorkerProvider>
  );
}
```

To allow the user to load a new version of the service worker (and optionally
the web app) add the useServiceWorker hook. The `waiting` property is true when
a new version of the service worker has been installed but not activated. To
activate the new version of the service worker invoke `skipWaiting`.

```tsx
import { useServiceWorker } from "service-worker-provider";

export function Page() {
  const { waiting, skipWaiting } = useServiceWorker();

  function handleClick(){
    skipWaiting();
  }

  return (
    {waiting ? <button onClick={handleClick}>Update</button> : null}
  );
}
```

### Update the service worker

The `useServiceWorker` hook returns a function named `update` that takes no arguments and has no
return value. Invoke this function to [check for an
update](https://web.dev/articles/service-worker-lifecycle#manual_updates). There are [limits to how
often update will work](https://web.dev/articles/service-worker-lifecycle#updates).

### Skip waiting to update

If you want to enable the `skipWaiting` functionality in useServiceWorker you
will need to add code similar to the following example to your service worker.
The service worker must listen for a message sent by service-worker-provider
(this package) that indicates to the service worker that it should invoke
`skipWaiting` and make the current worker the active worker.

In the example code the service worker adds a listener for a message event whose
`data` is the string "SKIP_WAITING". When that message is received invoke the
service worker's `skipWaiting` method. Once `skipWaiting` has completed post a
message object back with the properties `clientData` with `data` from the first
message as its value, and a `response` property with the value "COMPLETED". This
tells the service-worker-provider that the latest available service worker is
now active.

```ts
self.addEventListener("message", evt => {
  if (evt.data === "SKIP_WAITING") {
    self.skipWaiting().then(() => {
      evt.source.postMessage({
        clientData: evt.data,
        response: "COMPLETED"
      });
    });
  }
});
```

## Publish

- Bump the libs/service-worker-provider/package.json version field.
- `rm -rf dist`
- `yarn build service-worker-provider --skipNxCache`
- `cd dist/libs/service-worker-provider`
- `npm publish` | `npm publish --tag next`
