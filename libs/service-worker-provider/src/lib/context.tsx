import React from "react";

export const Context = React.createContext<ContextValue>({});

export interface ContextValue {
  /**
   * Invoking this method will cause a newly installed version of the service
   * worker to become the active service worker.
   * @description Typically this is invoked by a button on the UI clicked by the
   * user in response to a message indicating a new version of the "application"
   * is ready to be loaded.
   *
   * Causes the message "SKIP_WAITING" to be posted to the service worker. The
   * service worker should call its own `skipWaiting` then after it completes
   * post a `Completed` message back to the client.
   * @see {@link Completed}
   */
  skipWaiting?: () => void;
  /**
   * Request that the service worker do an update check for a new version of the service worker.
   */
  update?: () => void;
  /** If true there is a new service worker installed that is waiting to be
   * activated. */
  waiting?: boolean;
}
