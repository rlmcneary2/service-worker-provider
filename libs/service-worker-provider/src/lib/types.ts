/** The type of `data` the client will post to the service worker. */
export type ClientMessageData =
  | ClientMessageType
  | {
      /** The type of the client request. */
      request: ClientMessageType;
      /** An optional unique ID from the client request. */
      uid?: number | string;
    };

/** Define messages sent by the client to the service worker. */
export type ClientMessageType = "SKIP_WAITING";

/** A `data` object from the service worker that indicates it has completed a
 * request from the client. */
export interface Completed extends ServiceWorkerMessageData {
  response: "COMPLETED";
}

/** Defines the `data` property of a message sent by the service worker to the
 * client. */
export interface ServiceWorkerMessageData {
  /** The type of the client request. */
  clientData: ClientMessageType;
  /** The service worker's response. */
  response: ServiceWorkerMessageType;
  /** An optional unique ID from the client request. */
  uid?: number | string;
}

/** Define messages sent by the service worker to the client. */
export type ServiceWorkerMessageType = "COMPLETED";
